<?php declare(strict_types=1);

namespace App\Models;

use Ehann\RediSearch\Fields\NumericField;
use Ehann\RediSearch\Fields\TextField;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Post extends Model {
    use Searchable;

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'posts_index5';
    }

    public function toSearchableArray()
    {
        return [
            'name' => $this->name,
            'text' => $this->text,
            'uuid' => $this->id,
        ];
    }

    public function searchableSchema()
    {
        return [
            'name' => TextField::class,
            'text' => TextField::class,
            'uuid' => NumericField::class,
        ];
    }
}
