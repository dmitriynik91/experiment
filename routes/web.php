<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $redis = (new \Ehann\RedisRaw\PredisAdapter())->connect('core-auction-redis', 6379);

//    try {
//        $postIndex = new \Ehann\RediSearch\Index($redis, 'posts_index');
//        dd($postIndex->info());
//        $result = $postIndex
//            ->language(\Ehann\RediSearch\Language::RUSSIAN)
//            ->search('сеть');
//
////        $postIndex->drop();
//
//        dd($result);
//    } catch (\Ehann\RediSearch\Exceptions\UnknownIndexNameException $e) {



        $postIndex = new \Ehann\RediSearch\Index($redis, 'posts_index2');
        $postIndex->drop();

        $postIndex = new \Ehann\RediSearch\Index($redis);

        $postIndex
            ->setIndexName('posts_index2')
            ->addTextField('name')
            ->addTextField('text')
            ->addNumericField('uuid')
            ->addTagField('category')
            ->addTagField('type')
            ->create();

        foreach (\App\Models\Post::all() as $key => $post) {
            $postIndex->add([
                (new \Ehann\RediSearch\Fields\TextField('name', $post->name))->setWeight(5),
                (new \Ehann\RediSearch\Fields\TextField('text', $post->text))->setWeight(2),
                new \Ehann\RediSearch\Fields\NumericField('uuid', $post->id),
                new \Ehann\RediSearch\Fields\TagField('category', $key),
                new \Ehann\RediSearch\Fields\TagField('type', $key),
            ]);
        }
//
//    $document = $postIndex->makeDocument();
//    $document->name->setValue("Новое название");
//    $document->text->setValue("Новое название");
//    $document->uuid->setValue(23423);

//    $postIndex->add($document);

//    $result = $postIndex->language(\Ehann\RediSearch\Language::RUSSIAN)->search('управление сетями');
//    $result = $postIndex->numericFilter('@uuid', 2)->search();
//    $result = $postIndex->language(\Ehann\RediSearch\Language::RUSSIAN)->search("Спутник");
    $result = $postIndex->tagFilter('category', [0 ,2])->sortBy('category', 'desc')->search();
    dd($result);
//
//    $a = \App\Models\Post::search("управление сетями")->get();
//    dd($a);

    return view('welcome');
});
