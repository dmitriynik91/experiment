<?php

use Illuminate\Database\Seeder;

class FillTables extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('posts')->insert([
            'name' => "Всё о проекте «Спутниковый интернет Starlink».",
            'text' => "Центр управления сетью (ЦУС) обеспечивает управление",
        ]);

        \Illuminate\Support\Facades\DB::table('posts')->insert([
            'name' => "Теория и практика DIY-акустики: экспертные обзоры",
            'text' => "В этом дайджесте есть подробный теоретический блок спутник",
        ]);

        \Illuminate\Support\Facades\DB::table('posts')->insert([
            'name' => "Каждые двадцать шесть секунд: старые «микросейсмы» ",
            'text' => "Обе механизма работают, как часы. Хотя первый кажется с пути",
        ]);
    }
}
